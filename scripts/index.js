const url = 'https://ajax.test-danit.com/api/swapi/films';


fetch(url)

.then(response => response.json())
.then(res => {

    res.forEach(({episodeId, name, openingCrawl, id})=>{
        try{
            document.querySelector(`#container_${id}`).insertAdjacentHTML("beforeend", `<div class="card">
        <span>${episodeId} ${name} ${openingCrawl}</span>
        <div class="inside_${id}"></div>
        </div> `)


        }

        catch(err){
            console.error(err);
        }

    })

    res.forEach(({characters, id}) => {


            characters.forEach(el => {


                fetch(el)

                    .then(response => response.json())
                    .then(res =>

                        document.querySelector(`.inside_${id}`).insertAdjacentHTML("beforeend", `
         <li>${res.name}</li>`
      ))
                    }
        )
    })


})

.catch((err)=>{
    document.body.innerHTML = `<h1>Some Error: ${err.message}</h1>`
})



// {
//     "id": 2,
//     "characters": [
//     "https://ajax.test-danit.com/api/swapi/people/1",
//     "https://ajax.test-danit.com/api/swapi/people/2",
//     "https://ajax.test-danit.com/api/swapi/people/3",
//     "https://ajax.test-danit.com/api/swapi/people/4",
//     "https://ajax.test-danit.com/api/swapi/people/5",
//     "https://ajax.test-danit.com/api/swapi/people/10",
//     "https://ajax.test-danit.com/api/swapi/people/13",
//     "https://ajax.test-danit.com/api/swapi/people/14",
//     "https://ajax.test-danit.com/api/swapi/people/18",
//     "https://ajax.test-danit.com/api/swapi/people/20",
//     "https://ajax.test-danit.com/api/swapi/people/21",
//     "https://ajax.test-danit.com/api/swapi/people/22",
//     "https://ajax.test-danit.com/api/swapi/people/23",
//     "https://ajax.test-danit.com/api/swapi/people/24",
//     "https://ajax.test-danit.com/api/swapi/people/25",
//     "https://ajax.test-danit.com/api/swapi/people/26"
// ],
//     "director": "Irvin Kershner",
//     "episodeId": 5,
//     "openingCrawl": "It is a dark time for the Rebellion. Although the Death Star has been destroyed, Imperial troops have driven the Rebel forces from their hidden base and pursued them across the galaxy.\r\nEvading the dreaded Imperial Starfleet, a group of freedom fighters led by Luke Skywalker has established a new secret base on the remote ice world of Hoth.\r\nThe evil lord Darth Vader, obsessed with finding young Skywalker, has dispatched thousands of remote probes into the far reaches of space...",
//     "planets": [
//     "https://ajax.test-danit.com/api/swapi/planets/4",
//     "https://ajax.test-danit.com/api/swapi/planets/5",
//     "https://ajax.test-danit.com/api/swapi/planets/6",
//     "https://ajax.test-danit.com/api/swapi/planets/27"
// ],
//     "producer": "Gary Kurtz, Rick McCallum",
//     "releaseDate": "1980-05-17",
//     "species": [
//     "https://ajax.test-danit.com/api/swapi/species/1",
//     "https://ajax.test-danit.com/api/swapi/species/2",
//     "https://ajax.test-danit.com/api/swapi/species/3",
//     "https://ajax.test-danit.com/api/swapi/species/6",
//     "https://ajax.test-danit.com/api/swapi/species/7"
// ],
//     "starships": [
//     "https://ajax.test-danit.com/api/swapi/starships/3",
//     "https://ajax.test-danit.com/api/swapi/starships/10",
//     "https://ajax.test-danit.com/api/swapi/starships/11",
//     "https://ajax.test-danit.com/api/swapi/starships/12",
//     "https://ajax.test-danit.com/api/swapi/starships/15",
//     "https://ajax.test-danit.com/api/swapi/starships/17",
//     "https://ajax.test-danit.com/api/swapi/starships/21",
//     "https://ajax.test-danit.com/api/swapi/starships/22",
//     "https://ajax.test-danit.com/api/swapi/starships/23"
// ],
//     "name": "The Empire Strikes Back",
//     "url": "https://ajax.test-danit.com/api/swapi/films/2",
//     "vehicles": [
//     "https://ajax.test-danit.com/api/swapi/vehicles/14",
//     "https://ajax.test-danit.com/api/swapi/vehicles/16",
//     "https://ajax.test-danit.com/api/swapi/vehicles/18",
//     "https://ajax.test-danit.com/api/swapi/vehicles/19",
//     "https://ajax.test-danit.com/api/swapi/vehicles/20"
// ]
// },